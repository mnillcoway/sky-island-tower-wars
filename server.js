/**
 * Created by MniLL on 06.12.14.
 */

var nStatic = require('node-static');
var http = require('http');
var file = new nStatic.Server('./');
var path = require('path');
var fs = require('fs');
var filePath = path.join(__dirname, "/peer-rtc-id-counter-pid.json");


var peerIdCounters = 0; //Special object, for unique id, even after server restarted; Saved in file at filepath

var app = http.createServer(function (req, res) {
    file.serve(req, res);
}).listen(8887);

var clientsPool = [];
var io = require('socket.io').listen(app);

io.sockets.on('connection', function (socket) {
    socket.on('join', function (selectedClass) {
        if (!socket.pooled){
            clientsPool.push(socket);
            socket.pooled = true;
            if (clientsPool.length >=2)
                createGame();
        } else {
            console.log('Double connection');
        }
    });

    socket.on('roundData', function (data) {
        io.to(socket.enemy).emit('roundData', data);
    });

    socket.on('waiting', function (data) {
        io.to(socket.enemy).emit('waiting', data);
    });

    socket.on('disconnect', function() {
        if (socket.pooled) {
            if (clientsPool.indexOf(socket) !== -1){
                clientsPool.splice(clientsPool.indexOf(socket), 1);
                socket.pooled = false;
            }
        }
        if (socket.enemy) {
            io.to(socket.enemy).emit('disconnected', 'true');
        }
    });

    socket.peerId = getId();
    socket.emit('peerId', socket.peerId);
});

function createGame() {
    var client1, client2;
    if (clientsPool.length < 2)
        return false;

    client1 = clientsPool.pop();
    client2 = clientsPool.pop();

    let game = {
        peerId: client1.peerId,
        players: [{
            id: client1.peerId,
            base:{
                x: 2048 - 128,
                y: 1536 - 128,
            }
        }, {
            id: client2.peerId,
            base:{
                x: 128,
                y: 128,
            }
        }]
    };
    io.to(client1.id).emit('startGame', JSON.stringify(game));
    game.peerId = client2.peerId;
    io.to(client2.id).emit('startGame', JSON.stringify(game));
    client1.enemy = client2.id;
    client2.enemy = client1.id;
}

function getId() {
    return ++peerIdCounters;
}