
window.GlobalLogic =  function(withBot) {
    this.withBot = !!withBot;
    this.state = 'init'; //States init, search, connection, game, waiting, lose, win
    this.nextState = 'init';
    this.lastRound = -1;
    this.preRoundTick = 0;
    this.actions = [];
    this.waiting = 0;
    this.mainPlayer = null;
    this.init();
};

GlobalLogic.prototype.init = function(){
    this.players = [];
    this.energySources = [];
};

GlobalLogic.prototype.tick = function() {
    this.nextState && (this.state = this.nextState);
    this.nextState = null;
    var i = 0;
    switch (this.state) {
        case 'init':
            if (this.withBot)
                this.nextState = 'search';
            else {
                this.nextState = 'search';
                Game.socket.emit('join');
                this.initLoadingScreen('Searh for game');
            }
            break;
        case 'search': case 'connection':
            if (this.withBot) {
                Game.lvl.initGui();
                this.nextState = 'game';
                this.mainPlayer = new Player(0, true, false, new Vec2(Game.lvl.width-128, Game.lvl.height-128));
                this.players.push(this.mainPlayer);
                this.players.push(new Player(1, false, true, new Vec2(128, 128)));
                this.players[0].regroupPoint = this.players[0].enemyBase =  this.players[1].basePosition.clone();
                this.players[1].regroupPoint = this.players[1].enemyBase = this.players[0].basePosition.clone();
                this.energySources.push(new EnergySource(768, 256));
                this.energySources.push(new EnergySource(Game.lvl.width - 768, Game.lvl.height - 256));
                this.energySources.push(new EnergySource(Game.lvl.width/2, Game.lvl.height/2));
                this.energySources.push(new EnergySource(Game.lvl.width - 256, 128));
                this.energySources.push(new EnergySource(256, Game.lvl.height - 128));
            } else {
                if (this.state === 'connection') {
                    this.nextState = 'game';
                }
            }
        break;
        case 'game':
            if (++this.preRoundTick >= 10) {
                let allReady = true;
                this.players.forEach(function (pl) {
                    if (!pl.roundData)
                        allReady = false;
                });
                if (!allReady) {
                    if (++this.waiting > 10) {
                        this.nextState = 'waiting';
                        this.initLoadingScreen('Waiting for player')
                    }
                    Game.socket.emit('waiting', this.enemyPlayer.id);
                } else {
                    this.preRoundTick = 0;
                    this.players.forEach(function (pl) {
                        pl.round();
                    });
                    if (!this.withBot) {
                        Game.socket.emit('roundData', JSON.stringify(this.mainPlayer.roundData));
                    }
                    Game.lvl.realUpdate();
                    Game.lvl.touch();
                }
                if (allReady) {
                    this.waiting = 0;
                    this.players.forEach(function (pl) {
                        pl.update();
                    });
                    this.energySources.forEach(function (e) {
                        e.update();
                    })
                }
                //check round data;
            } else {
                this.players.forEach(function (pl) {
                    pl.update();
                });
                this.energySources.forEach(function (e) {
                    e.update();
                });
                Game.lvl.realUpdate();
                Game.lvl.touch();
            }
            break;
        case 'waiting':
            let allReady = true;
            this.players.forEach(function (pl) {
                if (!pl.roundData)
                    allReady = false;
            });
            if (allReady) {
                this.text && Game.pool.pull(this.text);
                this.text = null;
                this.nextState = 'game';
            }
            break;
        case 'lose':
            this.initLoadingScreen(this.state);
            break;
        case 'win':
            this.initLoadingScreen(this.state);
            break;
        case 'disconnected':
            this.initLoadingScreen('Enemy disconnected');
            break;
    }
};

GlobalLogic.prototype.disconnected = function () {
    if (this.state !== 'lose' && this.state !== 'win')
        this.nextState = 'disconnected';
};

GlobalLogic.prototype.win = function () {
    if (this.state !== 'lose')
        this.nextState = 'win';
};

GlobalLogic.prototype.lose = function () {
    this.nextState = 'lose';
};

GlobalLogic.prototype.build = function (building, position) {
    this.mainPlayer.nextRoundData.push({
        name:'build',
        payload: {
            name: building.name,
            x: position.x,
            y: position.y
        }
    })
};

GlobalLogic.prototype.setRegroupTo = function (position) {
    this.mainPlayer.nextRoundData.push({
        name:'regroup',
        payload: {
            x: position.x,
            y: position.y
        }
    })
};

GlobalLogic.prototype.startGame = function (data) {
    this.text && Game.pool.pull(this.text);
    this.nextState = 'connection';
    Game.lvl.initGui();

    let mainPlayer = data.players.filter(function (pl) {
        return pl.id === data.peerId;
    }).pop();
    let enemy = data.players.filter(function (pl) {
        return pl.id !== data.peerId;
    }).pop();

    if (mainPlayer.id > enemy.id) {
        this.mainPlayer = new Player(mainPlayer.id, true, false, new Vec2(mainPlayer.base.x, mainPlayer.base.y));
        this.players.push(this.mainPlayer);
        this.enemyPlayer = new Player(enemy.id, false, false, new Vec2(enemy.base.x, enemy.base.y));
        this.players.push(this.enemyPlayer);
    } else {
        this.enemyPlayer = new Player(enemy.id, false, false, new Vec2(enemy.base.x, enemy.base.y));
        this.players.push(this.enemyPlayer);
        this.mainPlayer = new Player(mainPlayer.id, true, false, new Vec2(mainPlayer.base.x, mainPlayer.base.y));
        this.players.push(this.mainPlayer);
    }
    this.players[0].regroupPoint = this.players[0].enemyBase =  this.players[1].basePosition.clone();
    this.players[1].regroupPoint = this.players[1].enemyBase = this.players[0].basePosition.clone();

    this.energySources.push(new EnergySource(768, 256));
    this.energySources.push(new EnergySource(Game.lvl.width - 768, Game.lvl.height - 256));
    this.energySources.push(new EnergySource(Game.lvl.width/2, Game.lvl.height/2));
    this.energySources.push(new EnergySource(Game.lvl.width - 256, 128));
    this.energySources.push(new EnergySource(256, Game.lvl.height - 128));
};

GlobalLogic.prototype.connectionClose = function (connection) {

};

GlobalLogic.prototype.serverIsGone = function(){
    this.state = 'disconnected';
};

GlobalLogic.prototype.brodcast = function (data) {
};

GlobalLogic.prototype.onData = function (data) {
    this.enemyPlayer.roundData = data;
};


GlobalLogic.prototype.initLoadingScreen = function (Text) {
    this.text && Game.pool.pull(this.text);
    this.text = new PIXI.Text(Text, {
        font: Game.getFont(96),
        fill: "#FFFFFF"
    });
    this.text.anchor.set(0.5, 0.5);
    this.text.position = new Vec2(Game.windowWidth/2, Game.windowHeight/2);
    Game.lvl.guiContainer.addChild(this.text);
};