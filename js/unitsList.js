function loadUnitsList() {
    function scanEnemyAround(owner) {
        let enemy = Game.lvl.globalLogic.players.filter(function (pl) {
            return pl.id !== owner.id;
        }).pop();
        let target;
        !target && enemy.units.forEach(function (bl) {
            if (!target && bl.position.clone().sub(this.position).getLength() < this.range)
                target = bl;
        }.bind(this));
        enemy.buildings.forEach(function (bl) {
            if (!target && bl.position.clone().sub(this.position).getLength() < this.range)
                target = bl;
        }.bind(this));
        return target;
    }

    function simpleUnitStrategy() {
        this.cooldown -= Game.cycleTickTime;
        if (this.cooldown < 0) {
            this.cooldown += this.coolDownTime;
            if (!(this.enemy && !this.enemy.dead && this.enemy.position.clone().sub(this.position).getLength() < this.range)) {
                this.enemy = this.scanEnemyAround(this.owner);
            }
            this.enemy && this.owner.bullets.push(new Bullet(this.position.clone(), this.enemy, this));
            if (this.enemy)
                return;
        }
        if (!this.enemy || this.enemy.dead) {
            this.destionation = this.regroupReached ? this.owner.enemyBase : this.regroupPoint;
            this.direction = this.destionation.clone().sub(this.position).add(this.randomed);
            if (this.direction.getLength() < this.range/2) {
                this.regroupReached = true;
            }
            this.sprite.rotation = -1 * this.direction.getRotate();
            this.position.add(this.direction.normalize().multiplied(this.speed));
        }
    }


    return new Promise(function(resolve) {
        Game.unitsList.jeep = {
            name: 'jeep',
            sprite: {'you': Game.sprites.jeep, 'enemy': Game.sprites.jeepE},
            damage: 2,
            range: 2 * 64,
            width: 32,
            height: 32,
            hitPoints: 10,
            cost: 0,
            speed: 6,
            sellCost: 0,
            coolDownTime: 500,
            hitPosition: new Vec2(0, 0),
            hitRange: 32,
            transformer: true,
            onTouch: function (touch) {

            },
            strategy: function () {

            },
            onDeath: function () {},
            init: function (params) {
                this.scanEnemyAround = scanEnemyAround.bind(this);
                this.strategy = simpleUnitStrategy.bind(this);
            }
        };

        Game.unitsList.tank = {
            name: 'tank',
            sprite: {'you': Game.sprites.tank, 'enemy': Game.sprites.tankE},
            damage: 15,
            range: 3 * 64,
            width: 32,
            height: 32,
            hitPoints: 45,
            cost: 0,
            sellCost: 0,
            speed: 2,
            coolDownTime: 1000,
            hitPosition: new Vec2(0, 0),
            hitRange: 32,
            transformer: true,
            onTouch: function (touch) {

            },
            strategy: function () {

            },
            onDeath: function () {},
            init: function (params) {
                this.scanEnemyAround = scanEnemyAround.bind(this);
                this.strategy = simpleUnitStrategy.bind(this);
            }
        };


        resolve();
    })
}
