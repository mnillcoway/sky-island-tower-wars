/**
 * Created by ilya on 22/04/2017.
 */

window.Camera = function(renderObject){
    this.renderObject = renderObject;
    this.position = new Vec2 (Game.windowWidth/2, Game.windowHeight/2);
    this.zoom = 1;
    this.windowSize = new Vec2(Game.windowWidth/2, Game.windowHeight/2);
    this.changeRender();
};

Camera.prototype.changeRender = function(){
    this.renderObject.pivot = this.getShift();
    this.renderObject.scale.x = this.renderObject.scale.y = this.getScale();
};

Camera.prototype.getScale = function(){
    return this.zoom * Game.scale;
};

Camera.prototype.getShift = function(){
    return this.position.clone().sub(this.windowSize.clone().multiply(1/this.zoom)).multiply(1);
};

Camera.prototype.move = function(direction){
    this.position.add(direction.multiplyed(1/this.zoom));
    this.checkViewport();
    this.changeRender();
};

Camera.prototype.checkViewport = function(){
    let viewPortLeft = this.position.clone().sub(this.windowSize.multiplyed(1/this.zoom));
    let viewPortRight = this.position.clone().add(this.windowSize.multiplyed(1/this.zoom));
    if(viewPortLeft.x < 0) {
        this.position.x = this.windowSize.multiplyed(1/this.zoom).x;
    }
    if (viewPortLeft.y < 0) {
        this.position.y = this.windowSize.multiplyed(1/this.zoom).y;
    }

    if(viewPortRight.x > this.windowSize.x*2) {
        this.position.x = this.windowSize.x*2 - this.windowSize.multiplyed(1/this.zoom).x;
    }

    if (viewPortRight.y > Game.lvl.height) {
        this.position.y = Game.lvl.height - this.windowSize.multiplyed(1/this.zoom).y;
    }
};

Camera.prototype.fromGuiLayerToMapLayer = function (position) {
    return position.clone().multiply(1/this.getScale());
};
Camera.prototype.getMovedPosition = function(position){
    return position.clone().multiply(1/this.zoom).add(this.position.clone().sub(this.windowSize.multiplyed(1/this.zoom)));
};

Camera.prototype.getUnMovedPosition = function(position){
    return position.clone().sub(this.position.clone().sub(this.windowSize.multiplyed(1/this.zoom))).multiply(this.zoom);
};