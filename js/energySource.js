window.EnergySource = function (x, y) {
    this.position = new Vec2(x, y);
    this.sprite = Game.pool.getSprite('energySource');
    this.sprite.position = this.position;
    Game.lvl.mainContainer.addChild(this.sprite);
    this.energy = 4;
    this.counter = 0;
    this.connected = null;
    this.line = null;
};

EnergySource.prototype.update = function () {
    let self = this;
    if (this.connected && !this.connected.disabled && !this.connected.dead) {
        if (++this.counter > 100) {
            this.connected.owner.coins += this.energy;
            this.counter = 0;
            Game.lvl.effects.push(new CoinsEffect(this.position, this.energy));
        }
    } else {
        this.line && Game.pool.pull(this.line);
        this.connected = null;

        let connectTo = null;
        Game.lvl.globalLogic.players.forEach(function (pl) {
            pl.buildings.filter(function (bl) {
                return bl.transformer;
            }).forEach(function (bl) {
                if (!connectTo && !bl.dead && !bl.disabled && self.position.clone().sub(bl.position).getLength() < bl.range) {
                    connectTo = bl;
                }
            });
        });
        if (connectTo) {
            this.connected = connectTo;
            this.line = new PIXI.Graphics();
            this.line.lineStyle(2, 0xffffff);
            this.line.moveTo(0,0);
            this.line.lineTo(-1 * this.position.clone().sub(connectTo.position).x, -1 * this.position.clone().sub(connectTo.position).y);
            this.line.endFill();
            this.line.position = this.position;
            Game.lvl.effectContainer.addChild(this.line);
        }
    }
};