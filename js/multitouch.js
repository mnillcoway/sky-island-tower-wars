var multitouchInit = function (canvas) {

    function onStart (touchEvent) {
        // touchEvent.preventDefault();
    }

    document.addEventListener("touchstart", function(e){ onStart(e); }, false );
    document.addEventListener("touchmove", function(e){ onStart(e); }, false );
    document.addEventListener("touchend", function(e){ onStart(e); }, false );

    canvas.addEventListener("touchmove", function (touchEvent) {
        for (var i2 =0; i2 < touchEvent.changedTouches.length; i2 ++){
            for (var i =0; i < Game.touchList.length; i++) {
                if (Game.touchList[i].id === touchEvent.changedTouches[i2].identifier) {
                    Game.touchList[i].move(touchEvent.changedTouches[i2].pageX, touchEvent.changedTouches[i2].pageY);
                }
            }
        }
    }.bind(this));

    canvas.addEventListener("touchstart", function (touchEvent) {
        Game.touched = true;
        for (var i =0; i< touchEvent.changedTouches.length; i++){
            Game.touchList.push(new touch(touchEvent.changedTouches[i].pageX, touchEvent.changedTouches[i].pageY, touchEvent.changedTouches[i].identifier));
        }
    }.bind(this));

    canvas.addEventListener("touchend" , function (touchEvent) {
        for (var i2 =0; i2< touchEvent.changedTouches.length; i2 ++) {
            for (var i =0; i<Game.touchList.length; i++) {
                if (Game.touchList[i].id === touchEvent.changedTouches[i2].identifier) {Game.touchList[i].end();}
            }
        }
    }.bind(this));


    canvas.addEventListener("mousemove", function (mouseEvent) {
        if (Game.touchList.length>0){
            Game.touchList[0].move(mouseEvent.pageX, mouseEvent.pageY);
        }
    }.bind(this));

    canvas.addEventListener("mousedown", function (mouseEvent) {
        onStart(mouseEvent);
        if (Game.noTouch < 1)
            Game.touchList.push(new touch(mouseEvent.pageX, mouseEvent.pageY, 0));
    }.bind(this));

    canvas.addEventListener("mouseup" , function (mouseEvent) {
        if (Game.touchList.length>0){
            for (var i=0; i< Game.touchList.length; i++)
                Game.touchList[i].end();
        }
    }.bind(this));

    function keyHandler(event) {
        if (event.keyCode === 32) {
            Game.lvl && Game.lvl && Game.lvl.objGui && Game.lvl.objGui.filter(function (pl) {
                return pl.params.subType === 'buildUnit' || pl.params.subType === 'buildBuilding';
            }).forEach(function (pl) {
                pl.show();
            })
            // onStart(event);
            // if (Game.lvl.loseGui.showed)
            //     Game.lvl.loseGui.hide();
        }
        // var keyPressed = String.fromCharCode(event.keyCode);
        // console.log(keyPressed);
    }
    var realWindow = window.parent || window;
    realWindow.addEventListener("keydown", keyHandler, false);
};


var touch = function  (_ex, _ey, _id){

    this.id= _id;
    this.startPosition = new Vec2(_ex, _ey);
    this.lastPosition = new Vec2(_ex, _ey);
    this.touchedEnd = false;

    this.move = function (newX, newY){
        this.lastPosition.x = newX;
        this.lastPosition.y = newY;
    };

    this.end = function (){
        this.touchedEnd = true;
    };

    this.del = function () {
        Game.touchList.delEl(this);
    }
};
