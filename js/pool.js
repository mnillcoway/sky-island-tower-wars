/**
 * Just a sprites and animations pool manager.
 * @constructor
 */
var Pool = function() {
    /**
     * @private
     * @type {Object.<string, PIXI.extras.MovieClip[]>}
     */
    this.animations = {};

    /**
     * @private
     * @type {Object.<string, PIXI.Sprite[]>}
     */
    this.sprites = {};
};

/**
 * pull object to pool
 * @param obj{*}
 */
Pool.prototype.pull = function(obj) {
    if (obj instanceof PIXI.extras.MovieClip) {
        this.pullAnimation(obj);
    } else if (obj instanceof PIXI.Sprite) {
        obj.scale.set(1, 1); //set scale to default;
        obj.alpha = 1; //set alpha to default;
        obj.rotation = 0; //set rotation to default;
        this.pullSprite(obj);
    } else {
        if (obj && obj.parent)
            obj.parent.removeChild(obj);
    }
};

/**
 * Get sprite from pool or create new one
 * @param name {string}
 * @returns {PIXI.Sprite}
 */
Pool.prototype.getSprite = function(name) {
    var sprite;

    if (!this.sprites[name])
        this.sprites[name] = [];

    if (this.sprites[name].length > 0) {
        sprite = this.sprites[name].pop();
    } else {
        sprite = PIXI.Sprite.fromFrame(Game.sprites[name].src);
        if (Game.sprites[name].anchor)
            sprite.anchor = Game.sprites[name].anchor.clone();

        if (Game.sprites[name].pivot)
            sprite.pivot = Game.sprites[name].pivot.clone();
    }
    sprite.name = name;
    return sprite;
};

/**
 * pull sprite to pool
 * @private
 * @param sprite {PIXI.Sprite}
 */
Pool.prototype.pullSprite = function(sprite){
    if (!this.sprites[sprite.name])
        this.sprites[sprite.name] = [];

    this.sprites[sprite.name].push(sprite);
    if (sprite.parent)
        sprite.parent.removeChild(sprite);
};

/**
 * Switch sprite to new from pool. New sprite has been inserted to old sprite place in parent.
 *
 * @returns {PIXI.Sprite}
 */
Pool.prototype.switchSprite = function (oldSprite, name){
    if (oldSprite.name === name.name)
        return oldSprite;

    var newSprite = this.getSprite(name);
    newSprite.position = oldSprite.position;
    newSprite.currentFrame = 0;
    if (oldSprite.parent)
        oldSprite.parent.addChildAt(newSprite, oldSprite.parent.children.indexOf(oldSprite));
    this.pullSprite(oldSprite);
    return newSprite;
};

/**
 *
 * @param name {string}
 * @returns {PIXI.extras.MovieClip}
 */
Pool.prototype.getAnimation = function(name) {
    var anim;

    if (!this.animations[name])
        this.animations[name] = [];

    if (this.animations[name].length > 0) {
        anim = this.animations[name].pop();
        anim.currentFrame = 0;
    } else {
        anim = PIXI.extras.MovieClip.fromFrames(Game.animations[name].sprites);
        if (Game.animations[name].anchor)
            anim.anchor = Game.animations[name].anchor;
        anim.animationSpeed = 1000/Game.animations[name].playTime;
        anim.name = name;
    }
    anim.play();
    return anim;
};

/**
 * pull sprite to pool
 * @private
 * @param animation {PIXI.extras.MovieClip}
 */
Pool.prototype.pullAnimation = function(animation){
    if (!this.animations[animation.name])
        this.animations[animation.name] = [];

    animation.stop();
    animation.onComplete = null;
    this.animations[animation.name].push(animation);

    if (animation.parent)
        animation.parent.removeChild(animation);
};

/**
 * Switch animation to new from pool. New animation has been inserted to old animation place in parent.
 *
 * @param name {string} name of new sprite
 * @param anim {PIXI.extras.MovieClip} anim to switch
 * @returns {PIXI.extras.MovieClip}
 */
Pool.prototype.switchAnimation = function(name, anim){
    if (anim.name === name)
        return anim;

    var newAnim = this.getAnimation(name);
    newAnim.position = anim.position;
    newAnim.onComplete = anim.onComplete;
    newAnim.currentFrame = 0;

    if (anim.parent)
        anim.parent.addChildAt(newAnim, anim.parent.children.indexOf(anim));

    this.pullAnimation(anim);
    return newAnim;
};