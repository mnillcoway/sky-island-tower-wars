function Building(params, position, owner, id) {
    this.id = id;
    this.params = params;
    this.position = position.clone();
    this.owner = owner;

    if (!params.sprite.playTime)
        this.sprite = Game.pool.getSprite(params.sprite[owner.isMain ? 'you' : 'enemy'].name);
    else
        this.sprite = Game.pool.getAnimation(params.sprite[owner.isMain ? 'you' : 'enemy'].name);

    this.sprite.position = this.position;
    Game.lvl.mainContainer.addChild(this.sprite);

    if (params.transformer) {
        this.range = params.range;
        this.transformer = true;
        this.transformerRange = new PIXI.Graphics();
        this.transformerRange.beginFill('rgba(0, 255, 0)', 1);
        this.transformerRange.drawCircle(0, 0, this.range);
        this.transformerRange.endFill();
        this.transformerRange.alpha = 0;
        this.transformerRange.position = this.position;
        Game.lvl.transformerContainer.addChild(this.transformerRange);
    }

    this.type = params.type;
    this.dead = false;

    this.hitPoints = params.hitPoints;
    this.maxHitPoints = params.hitPoints;

    this.hitPosition = params.hitPosition || new Vec2(0,0);
    this.hitRange = params.hitRange || 0;

    this.radius = Math.max(params.width, params.height);
    this.width = params.width;
    this.height = params.height;

    this.onDeath = params.onDeath;
    this.strategy = params.strategy;

    this.init();
    if (params.init)
        params.init.call(this, params);
}

Building.prototype.init  = function () {

};

Building.prototype.update  = function () {
    if (!this.dead && !this.disabled) {
        this.strategy(this);
        if (this.maxHitPoints > this.hitPoints) {
            this.hitPoints += this.maxHitPoints * 0.01;
        }
    }
};


Building.prototype.applyDamage = function (damage, source) {
    if (!this.dead) {
        this.hitPoints -= damage;
        if (this.hitPoints <= 0)
            this.death();
    }
};

Building.prototype.del = function (damage) {
    this.owner.buildings.delEl(this);
    Game.pool.pull(this.sprite);
    this.transformer && Game.pool.pull(this.transformerRange);
    this.disabledEffect && Game.pool.pull(this.disabledEffect);
    this.owner.recalculateEnergy(this.transformer);
};

Building.prototype.death = function () {
    this.dead = true;
    if (this.onDeath)
        this.onDeath();
    this.del();
};

Building.prototype.showDisableEffect = function () {
    if (!this.disabledEffect){
        this.disabledEffect = Game.pool.getSprite('particle');
        this.disabledEffect.position = this.position;
        Game.lvl.mainContainer.addChild(this.disabledEffect);
    }
};

Building.prototype.hideDisableEffect = function () {
    this.disabledEffect && Game.pool.pull(this.disabledEffect);
    this.disabledEffect = null;
};
