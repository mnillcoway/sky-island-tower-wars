function Phantom(building, position, range) {
    this.container = new PIXI.Container();

    this.building = building;
    if (!building.sprite.playTime)
        this.sprite = Game.pool.getSprite(building.sprite['you'].name);
    else
        this.sprite = Game.pool.getAnimation(building.sprite['you'].name);

    if (building.range && building.range > 0) {
        this.transformerRange = new PIXI.Graphics();
        this.transformerRange.beginFill('rgba(0, 255, 0)', 0.2);
        this.transformerRange.drawCircle(0, 0, building.range);
        this.transformerRange.endFill();
        this.container.addChild(this.transformerRange);
    }

    this.sprite.position = new Vec2(0,0);
    this.container.addChild(this.sprite);
    this.container.position = position.clone();
}

function Bullet(position, target, parent, sprite) {
    if (!sprite) {
        this.sprite = Game.pool.getSprite('particle');
        this.sprite.scale.x = this.sprite.scale.y = 0.2;
    } else {
        if (!sprite.playTime)
            this.sprite = Game.pool.getSprite(sprite.name);
        else
            this.sprite = Game.pool.getAnimation(sprite.name);
    }
    this.sprite.position = position.clone();
    this.target = target;
    this.parent = parent;
    Game.lvl.effectContainer.addChild(this.sprite);
}

Bullet.prototype.update = function () {
    this.targetPosition = this.target.position.clone();
    this.direction = this.sprite.position.clone().sub(this.targetPosition);
    if (this.direction.getLength() < 32) {
        this.sprite.position = this.targetPosition;
        this.target.applyDamage(this.parent.damage, this.parent);
        this.parent.owner.bullets.delEl(this);
        Game.pool.pull(this.sprite);
    } else {
        this.sprite.position.sub(this.direction.normalize().multiplied(32));
    }
};

function CoinsEffect(position, count) {
    this.counter = 0;
    this.text = new PIXI.Text('+' + count, {
        font: Game.getFont(36),
        fill: "#FFFFFF"
    });
    this.text.anchor.set(0.5, 0.5);
    this.text.position = position.clone();
    Game.lvl.effectContainer.addChild(this.text);
}

CoinsEffect.prototype.update = function () {
    if (++this.counter < 15) {
        this.text.position.y -= 3;
    } else {
        Game.pool.pull(this.text);
        Game.lvl.effects.delEl(this);
    }
};