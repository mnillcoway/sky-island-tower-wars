function Player(id, isMain, isBot, basePosition) {
    this.id = id;
    this.isMain = isMain;
    this.isBot = isBot;

    this.roundData = [];
    this.nextRoundData = [];
    this.coins = Game.startCoins;
    this.coinsCounter = 0;

    this.basePosition = basePosition.clone();

    this.buildings = [];
    this.units = [];
    this.bullets = [];
    this.idCounter = 0;

    this.regroupPoint = this.basePosition.clone();
    this.buildings.push(new Building(Game.buildingsList.base, this.basePosition, this));
}

Player.prototype.update = function() {
    if (this.lose)
        return;

    let i;
    for (i = this.buildings.length - 1; i >=0; i--) {
        this.buildings[i].update();
    }
    for (i = this.bullets.length - 1; i >=0; i--) {
        this.bullets[i].update();
    }

    for (i = this.units.length - 1; i >=0; i--) {
        this.units[i].update();
    }

    if (++this.coinsCounter > 100) {
        this.coinsCounter = 0;
        this.coins += 4;
        Game.lvl.effects.push(new CoinsEffect(this.basePosition, 4));
    }
};

Player.prototype.round = function() {
    let self = this;
    this.roundData.forEach(function (action) {
        switch (action.name) {
            case 'build':
                if ('payload' in action && !isNaN(action.payload.x) && !isNaN(action.payload.y) && 'name' in action.payload && action.payload.name in Game.buildingsList && Game.buildingsList.hasOwnProperty(action.payload.name)) {
                    let buildParams = Game.buildingsList[action.payload.name];
                    let position = new Vec2(action.payload.x, action.payload.y);
                    if (self.canBeBuild(buildParams, position)) {
                        let building = new Building(buildParams, position, self, ++this.idCounter);
                        self.buildings.push(building);
                        self.coins -= buildParams.cost;
                        if (building.transformer) {
                            self.recalculateEnergy(true);
                        }
                    }
                }
                break;
            case 'regroup':
                if ('payload' in action && !isNaN(action.payload.x) && !isNaN(action.payload.y)) {
                    let position = new Vec2(action.payload.x, action.payload.y);
                    if (position.x > 0 && position.x < Game.lvl.width && position.y > 0 && position.y < Game.lvl.height) {
                        self.regroupPoint = new Vec2(position.x, position.y);
                    }
                }
                break;
        }
    });
    if (this.isMain) {
        this.roundData = this.nextRoundData;
        this.nextRoundData = [];
    } else if (!this.isBot) {
        this.roundData = null;
    }
};

Player.prototype.canBeBuild = function (buildParams, position) {
    let self = this;
    if (self.coins > buildParams.cost && position.x > buildParams.width/2 && position.x < Game.lvl.width - buildParams.width/2 && position.y > buildParams.height/2 && position.y < Game.lvl.height - buildParams.height/2) {
        let canBeBuild = true;
        let inRange = false;
        let radius = Math.max(buildParams.width, buildParams.height);
        Game.lvl.globalLogic.players.forEach(function (pl) {
            pl.buildings.forEach(function (bl) {
                if (bl.position.clone().sub(position).getLength() < Math.max(radius, bl.radius)) {
                    canBeBuild = false;
                }
            })
        });
        self.buildings.forEach(function (bl) {
            if (bl.transformer && bl.range > bl.position.clone().sub(position).getLength() - radius/4) {
                inRange = true;
            }
        });
        return canBeBuild && inRange
    }
};

Player.prototype.recalculateEnergy = function (recalculateTransformers) {
    if (recalculateTransformers) {
        let transformers = this.buildings.filter(function (bl) {
            return bl.transformer;
        });
        let newDisabled = true;
        while (newDisabled) {
            newDisabled = false;
            transformers.forEach(function (bl) {
                if (bl.params.name !== 'base' && !bl.disabled) {
                    if (!bl.connectedFrom || bl.connectedFrom.disabled || bl.connectedFrom.hitPoints <= 0) {
                        bl.connectedFrom = null;
                        bl.line && Game.pool.pull(bl.line);
                        bl.line = null;
                        bl.disabled = true;
                        newDisabled = true;
                    }
                }
            });
        }
        let hasNotConnected = true;
        while (hasNotConnected) {
            let notConnected = transformers.filter(function (bl) {
                return bl.disabled;
            });
            hasNotConnected = false;
            notConnected.forEach(function (bl_n) {
                transformers.reverse().forEach(function (bl) {
                    if (bl_n.disabled && !bl.disabled && bl !== bl_n && bl.position.clone().sub(bl_n.position).getLength() - bl_n.radius/4 < bl.range) {
                        bl_n.connectedFrom = bl;
                        bl_n.disabled = false;
                        hasNotConnected = true;
                        bl_n.line = new PIXI.Graphics();
                        bl_n.line.lineStyle(2, 0xffffff);
                        bl_n.line.moveTo(0,0);
                        bl_n.line.lineTo(-1 * bl_n.position.clone().sub(bl.position).x, -1 * bl_n.position.clone().sub(bl.position).y);
                        bl_n.line.endFill();
                        bl_n.line.position = bl_n.position;
                        Game.lvl.effectContainer.addChild(bl_n.line);
                    }
                })
            })
        }

        transformers.forEach(function (bl) {
            bl.disabled && bl.showDisableEffect();
            !bl.disabled && bl.hideDisableEffect();
        });

        this.buildings.filter(function (bl) {
            return !bl.transformer;
        }).forEach(function (bl) {
            let worked = false;
            transformers.forEach(function (bl_t) {
                if (bl_t.position.clone().sub(bl.position).getLength()  - bl_t.radius/4 < bl_t.range) {
                    worked = true;
                }
            });
            if (!worked) {
                bl.disabled = true;
                bl.showDisableEffect();
            } else {
                bl.hideDisableEffect();
            }
        });
    }
};

Player.prototype.onLose = function () {
    if (!this.lose) {
        this.lose = true;
        for (let i = this.bullets.length - 1; i >= 0; i--) {
            Game.pool.pull(this.bullets[i].sprite);
            this.bullets.delEl(this.bullets[i]);
        }
        if (this === Game.lvl.globalLogic.mainPlayer) {
            Game.lvl.globalLogic.lose();
        } else {
            Game.lvl.globalLogic.win();
        }
    }
};