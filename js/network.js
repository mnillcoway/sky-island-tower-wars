/**
 * Created by MniLL on 06.12.14.
 */
(function(){
    Game.socket = io.connect(window.location.origin, {port: 8887});
    Game.socket.on('startGame', function (message) {
        Game.gameOptions = JSON.parse(message);
        Game.lvl.globalLogic.startGame(Game.gameOptions);
    });

    Game.socket.on('peerId', function(message){
        Game.socketId = parseInt(message);
    });

    Game.socket.on('roundData', function(message) {
        Game.lvl.globalLogic.onData(JSON.parse(message));
    });

    Game.socket.on('disconnected', function(message) {
        Game.lvl.globalLogic.disconnected(message);
    });

    Game.socket.on('waiting', function(message) {
        if (Game.inWaiting <= 0)
            Game.inWaiting = 50;
    });

})();