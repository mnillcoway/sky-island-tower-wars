function loadPrefsAndAssets () {
    return loadSpritesList()
        .then(loadSpritesImages)
        .then(loadBuildingsList)
        .then(loadUnitsList);
}

function loadLoadingScreen () {
    return new Promise(function(resolve,reject){
        //var loader = new PIXI.loaders.Loader();
        //loader.add('menuBackground', 'images/loading.jpg');

        //loader.once('complete' , function(loader, resources) {
        //    console.log('Loading screen loaded');
        //    callback(PIXI.Sprite.fromFrame('images/loading.jpg'));
        //});
        //loader.load();
        resolve();
    })
}

function loadSpritesImages () {
    Game.getFont = function (px) {
        return px + "px Arial";
    };
    return new Promise(function(resolve, reject) {
        var loader = new PIXI.loaders.Loader(), rejected = false;

        for (var key in Game.sprites) {
            if (Game.sprites.hasOwnProperty(key))
                loader.add(Game.sprites[key].name, Game.sprites[key].src);
        }

        loader.once('complete', function (loader, resources) {
            if (!rejected)
                resolve();
        });

        loader.once('error', function (loader, resources) {
            reject();
        });
        loader.load();
    });
}

function loadSpritesList() {
    return new Promise(function(resolve, reject){
        Game.sprites['particle'] = {name: 'particle', src:'images/particle.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['grass'] = {name: 'grass', src:'images/grass.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['base'] = {name: 'base', src:'images/base.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['baseE'] = {name: 'baseE', src:'images/baseE.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['button'] = {name: 'button', src:'images/button.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['buttonPressed'] = {name: 'buttonPressed', src:'images/buttonPressed.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['buttonBuildBuildings'] = {name: 'buttonBuildBuildings', src:'images/buttonBuildBuildings.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['buttonBuildTransformer'] = {name: 'buttonBuildTransformer', src:'images/buttonBuildTransformer.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['transformer'] = {name: 'transformer', src:'images/transformer.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['transformerE'] = {name: 'transformerE', src:'images/transformerE.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['buttonBuildBasicTurrer'] = {name: 'buttonBuildBasicTurrer', src:'images/buttonBuildBasicTurrer.png', anchor:new Vec2(0.5,0.5)};

        Game.sprites['basicTurret'] = {name: 'basicTurret', src:'images/basicTurret.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['basicTurretE'] = {name: 'basicTurretE', src:'images/basicTurretE.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['basicTurretTop'] = {name: 'basicTurretTop', src:'images/basicTurretTop.png', anchor:new Vec2(0.5,0.5)};

        Game.sprites['teslaTurret'] = {name: 'teslaTurret', src:'images/teslaTurret.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['teslaTurretE'] = {name: 'teslaTurretE', src:'images/teslaTurretE.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['buttonBuildTeslaTurret'] = {name: 'buttonBuildTeslaTurret', src:'images/buttonBuildTeslaTurret.png', anchor:new Vec2(0.5,0.5)};

        Game.sprites['cannonTurret'] = {name: 'cannonTurret', src:'images/cannonTurret.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['cannonTurretE'] = {name: 'cannonTurretE', src:'images/cannonTurretE.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['cannonTurretTop'] = {name: 'cannonTurretTop', src:'images/cannonTurretTop.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['buttonBuildCannonTurret'] = {name: 'buttonBuildCannonTurret', src:'images/buttonBuildCannonTurret.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['cannonBullet'] = {name: 'cannonBullet', src:'images/cannonBullet.png', anchor:new Vec2(0.5,0.5)};

        Game.sprites['buttonBuildUnits'] = {name: 'buttonBuildUnits', src:'images/buttonBuildUnits.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['buttonBuildJeepFabric'] = {name: 'buttonBuildJeepFabric', src:'images/buttonBuildJeepFabric.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['buttonBuildTankFabric'] = {name: 'buttonBuildTankFabric', src:'images/buttonBuildTankFabric.png', anchor:new Vec2(0.5,0.5)};

        Game.sprites['jeepFabric'] = {name: 'jeepFabric', src:'images/jeepFabric.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['jeepFabricE'] = {name: 'jeepFabricE', src:'images/jeepFabricE.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['tankFabric'] = {name: 'tankFabric', src:'images/tankFabric.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['tankFabricE'] = {name: 'tankFabricE', src:'images/tankFabricE.png', anchor:new Vec2(0.5,0.5)};

        Game.sprites['jeep'] = {name: 'jeep', src:'images/jeep.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['jeepE'] = {name: 'jeepE', src:'images/jeepE.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['tank'] = {name: 'tank', src:'images/tank.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['tankE'] = {name: 'tankE', src:'images/tankE.png', anchor:new Vec2(0.5,0.5)};

        Game.sprites['regroupPoint'] = {name: 'regroupPoint', src:'images/regroupPoint.png', anchor:new Vec2(0.5, 1)};
        Game.sprites['energySource'] = {name: 'energySource', src:'images/energySource.png', anchor:new Vec2(0.5, 0.5)};

        resolve();
    })
}