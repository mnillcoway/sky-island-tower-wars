function Unit(params, position, owner) {
    this.params = params;
    this.position = position.clone();
    this.owner = owner;

    if (!params.sprite.playTime)
        this.sprite = Game.pool.getSprite(params.sprite[owner.isMain ? 'you' : 'enemy'].name);
    else
        this.sprite = Game.pool.getAnimation(params.sprite[owner.isMain ? 'you' : 'enemy'].name);

    this.sprite.position = this.position;
    Game.lvl.mainContainer.addChild(this.sprite);
    this.range = params.range;
    this.dead = false;
    this.hitPoints = params.hitPoints;
    this.maxHitPoints = params.hitPoints;
    this.radius = Math.max(params.width, params.height);
    this.width = params.width;
    this.height = params.height;
    this.onDeath = params.onDeath;
    this.strategy = params.strategy;
    this.damage = params.damage;
    this.speed = params.speed;
    this.regroupReached = false;
    this.regroupPoint = owner.regroupPoint.clone();
    this.coolDownTime = params.coolDownTime;
    this.cooldown = 0;
    this.randomed = new Vec2(-this.range/2 + myRandom(0, this.range-2), -this.range/2 + myRandom(0, this.range-2));

    if (params.init)
        params.init.call(this, params);
}

Unit.prototype.init  = function () {

};

Unit.prototype.update  = function () {
    if (this.hitPoints > 0 && !this.disabled)
        this.strategy(this);
};


Unit.prototype.applyDamage = function (damage, source) {
    if (!this.dead) {
        this.hitPoints -= damage;
        if (this.hitPoints <= 0)
            this.death();
    }
};

Unit.prototype.del = function (damage) {
    this.owner.units.delEl(this);
    Game.pool.pull(this.sprite);
    this.special && Game.pool.pull(this.special);
};

Unit.prototype.death = function () {
    this.dead = true;
    if (this.onDeath)
        this.onDeath();
    this.del();
};