function scanEnemyAround(owner) {
    let enemy = Game.lvl.globalLogic.players.filter(function (pl) {
       return pl.id !== owner.id;
    }).pop();
    let target;
    enemy.buildings.forEach(function (bl) {
        if (!target && bl.position.clone().sub(this.position).getLength() < this.range)
            target = bl;
    }.bind(this));
    !target && enemy.units.forEach(function (bl) {
        if (!target && bl.position.clone().sub(this.position).getLength() < this.range)
            target = bl;
    }.bind(this));
    return target;
}

function simpleTowerStrategy() {
    this.cooldown -= Game.cycleTickTime;
    if (this.cooldown < 0) {
        this.cooldown += this.coolDownTime;
        let enemy = this.scanEnemyAround(this.owner);
        if (enemy) {
            this.special && (this.special.rotation = -1 * this.position.clone().sub(enemy.position).getRotate());
            this.owner.bullets.push(new Bullet(this.position.clone(), enemy, this, this.params.bulletSprite))
            //TODO shoot
        }
    }
}

function loadBuildingsList() {
    return new Promise(function(resolve) {
        Game.buildingsList.base = {
            name: 'base',
            sprite: {'you': Game.sprites.base, 'enemy': Game.sprites.baseE},
            damage: 10,
            range: 8 * 64,
            icon: {normal: 'baseIcon', disabled: 'baseIconD'},
            width: 64,
            height: 64,
            hitPoints: 200,
            cost: 0,
            sellCost: 0,
            coolDownTime: 500,
            hitPosition: new Vec2(0, 0),
            hitRange: 32,
            transformer: true,
            onTouch: function (touch) {},
            strategy: function () {},
            onDeath: function () {
                this.owner.onLose();
            },
            init: function (params) {
                this.range = params.range;
                this.damage = params.damage;
                this.coolDownTime = params.coolDownTime;
                this.cooldown = 0;
                this.scanEnemyAround = scanEnemyAround.bind(this);
                this.strategy = simpleTowerStrategy.bind(this);
            }
        };

        Game.buildingsList.transformer = {
            name: 'transformer',
            sprite: {'you': Game.sprites.transformer, 'enemy': Game.sprites.transformerE},
            range: 4 * 64,
            width: 32,
            height: 32,
            hitPoints: 25,
            cost: 50,
            sellCost: 0,
            transformer: true,
            hitPosition: new Vec2(0, 0),
            onTouch: function (touch) {},
            strategy: function () {
            },
            onDeath: function () {
                this.line && Game.pool.pull(this.line);
            },
            init: function (params) {
                this.range = params.range;
            }
        };

        Game.buildingsList.basicTurret = {
            name: 'basicTurret',
            sprite: {'you': Game.sprites.basicTurret, 'enemy': Game.sprites.basicTurretE},
            range: 3 * 64,
            width: 32,
            height: 32,
            hitPoints: 30,
            cost: 30,
            sellCost: 0,
            damage: 5,
            coolDownTime: 1000,
            transformer: false,
            hitPosition: new Vec2(0, 0),
            onTouch: function (touch) {},
            strategy: function () {

            },
            onDeath: function () {

            },
            init: function (params) {
                this.special = Game.pool.getSprite('basicTurretTop');
                this.special.position = this.position;
                Game.lvl.mainContainer.addChild(this.special);
                this.range = params.range;
                this.damage = params.damage;
                this.coolDownTime = params.coolDownTime;
                this.cooldown = 0;
                this.scanEnemyAround = scanEnemyAround.bind(this);
                this.strategy = simpleTowerStrategy.bind(this);
            }
        };


        Game.buildingsList.teslaTurret = {
            name: 'teslaTurret',
            sprite: {'you': Game.sprites.teslaTurret, 'enemy': Game.sprites.teslaTurretE},
            range: 3 * 64,
            width: 32,
            height: 32,
            hitPoints: 30,
            cost: 50,
            sellCost: 0,
            damage: 100,
            coolDownTime: 2000,
            transformer: false,
            hitPosition: new Vec2(0, 0),
            onTouch: function (touch) {

            },
            strategy: function () {

            },
            onDeath: function () {

            },
            init: function (params) {

                this.range = params.range;
                this.damage = params.damage;
                this.coolDownTime = params.coolDownTime;
                this.cooldown = 0;
                this.scanEnemyAround = scanEnemyAround.bind(this);
                this.strategy = simpleTowerStrategy.bind(this);
            }
        };

        Game.buildingsList.cannonTurret = {
            name: 'cannonTurret',
            bulletSprite: Game.sprites.cannonBullet,
            sprite: {'you': Game.sprites.cannonTurret, 'enemy': Game.sprites.cannonTurretE},
            range: 6 * 64,
            width: 64,
            height: 64,
            hitPoints: 100,
            cost: 80,
            sellCost: 0,
            damage: 20,
            coolDownTime: 2000,
            transformer: false,
            hitPosition: new Vec2(0, 0),
            onTouch: function (touch) {

            },
            strategy: function () {

            },
            onDeath: function () {

            },
            init: function (params) {
                this.special = Game.pool.getSprite('cannonTurretTop');
                this.special.position = this.position;
                Game.lvl.mainContainer.addChild(this.special);
                this.range = params.range;
                this.damage = params.damage;
                this.coolDownTime = params.coolDownTime;
                this.cooldown = 0;
                this.scanEnemyAround = scanEnemyAround.bind(this);
                this.strategy = simpleTowerStrategy.bind(this);
            }
        };

        Game.buildingsList.basicUnitBuilding = {
            // name: 'basicBuilding',
            // sprite: {'you': Game.sprites.cannonTurret, 'enemy': Game.sprites.cannonTurretE},
            range: 0,
            width: 32,
            height: 32,
            // hitPoints: 30,
            // cost: 50,
            // sellCost: 0,
            // damage: 50,
            coolDownTime: 5000,
            transformer: false,
            hitPosition: new Vec2(0, 0),
            onTouch: function (touch) {},
            strategy: function () {
                this.cooldown -= Game.cycleTickTime;
                if (this.cooldown < 0) {
                    this.cooldown += this.coolDownTime;
                    this.owner.units.push(new Unit(Game.unitsList[this.params.unit], this.position, this.owner));
                }
            },
            onDeath: function () {},
            init: function (params) {
                this.regroupPoint = null;
                this.coolDownTime = params.coolDownTime;
                this.cooldown = 0;
            }
        };

        Game.buildingsList.jeepFabric = {
            hitPoints: 10,
            name: 'jeepFabric',
            unit: 'jeep',
            cost: 20,
            sprite: {'you': Game.sprites.jeepFabric, 'enemy': Game.sprites.jeepFabricE},
        };
        Game.buildingsList.jeepFabric.__proto__ = Game.buildingsList.basicUnitBuilding;

        Game.buildingsList.tankFabric = {
            hitPoints: 50,
            width: 64,
            height: 64,
            coolDownTime: 12000,
            unit: 'tank',
            name: 'tankFabric',
            cost: 80,
            sprite: {'you': Game.sprites.tankFabric, 'enemy': Game.sprites.tankFabricE},
        };
        Game.buildingsList.tankFabric.__proto__ = Game.buildingsList.basicUnitBuilding;

        resolve();
    })
}
