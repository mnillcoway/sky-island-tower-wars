/**
 * Created by Mnill on 12.12.15.
 */

function loadMainGui () {
    function getPriceText(price, color) {
        this.price = new PIXI.Text(price, {
            font: Game.getFont(16),
            fill: color
        });
        this.price.anchor.set(0.5, 0.5);
        this.price.position.y = 22;
        this.price._color = color;
        return this.price;
    }

    function checkPrice() {
        if (!this.hidden) {
            if (Game.lvl.globalLogic.mainPlayer.coins >= Game.buildingsList[this.params.building].cost) {
                if (this.price._color !== '#FFFFFF') {
                    Game.pool.pull(this.price);
                    this.price = getPriceText(Game.buildingsList[this.params.building].cost, '#FFFFFF');
                    this.guiContainer.addChild(this.price);
                }
            } else {
                if (this.price._color !== '#FF0000') {
                    Game.pool.pull(this.price);
                    this.price = getPriceText(Game.buildingsList[this.params.building].cost, '#FF0000');
                    this.guiContainer.addChild(this.price);
                }
            }
        }
    }

    return new Promise(function(resolve) {
        Game.guiList.start = {
            noLock:true,
            sprite : null,
            x: Game.windowWidth/2,
            y: Game.windowHeight/2,
            width: 184,
            height: 80,
            onTouch: function(touch) {},
            onUpdate: function(){},
            init:function() {
                this.guiContainer = new PIXI.Container();
                // this.guiContainer.scale.x = this.guiContainer.scale.y = 2;

                this.sprite = Game.pool.getSprite('button');

                this.guiContainer.addChild(this.sprite);
                this.guiContainer.position = this.position;

                this.text = new PIXI.Text('PLAY', {
                    font: Game.getFont(36),
                    fill: "#FFFFFF"
                });
                this.text.anchor.set(0.5, 0.5);
                this.guiContainer.addChild(this.text);

                this.guiContainer.interactive = true;
                this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
                    this.yestoched = true;
                    this.sprite = Game.pool.switchSprite(this.sprite, 'buttonPressed');
                }.bind(this);

                this.guiContainer.mouseup = this.guiContainer.touchend = this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
                    if (this.yestoched) {
                        this.yestoched = false;
                        this.sprite = Game.pool.switchSprite(this.sprite, 'button');
                        this.hide();
                    }
                }.bind(this);

                this.hide = function () {
                    this.startAnimation({startX: Game.windowWidth/2, toX: Game.windowWidth/2, startY: Game.windowHeight/2, toY: -180, iterations:7}, function() {
                        Game.isMenu = false;
                        Game.lvl.startGame();
                    })
                }
            }
        };

        Game.guiList.coins = {
            noLock:true,
            sprite : null,
            x: Game.windowWidth-12,
            y: 24,
            width: 0,
            height: 0,
            onTouch: function(touch) {},
            onUpdate: function(){
                this.text.text = '' + Game.lvl.globalLogic.mainPlayer.coins;
            },
            init:function() {
                this.guiContainer = new PIXI.Container();
                this.guiContainer.position = this.position;

                this.text = new PIXI.Text('' + Game.startCoins, {
                    font: Game.getFont(36),
                    fill: "#FFFFFF"
                });
                this.text.anchor.set(1, 0.5);
                this.guiContainer.addChild(this.text);

                this.hide = function () {
                    this.startAnimation({startX: this.params.x, toX: this.params.x, startY: this.params.y, toY: -180, iterations:7}, function() {
                        Game.isMenu = false;
                        Game.lvl.startGame();
                    })
                }
            }
        };

        Game.guiList.builBuildingsButton = {
            noLock:false,
            sprite : null,
            x: 64,
            y: Game.windowHeight-64,
            width: 64,
            height: 64,
            onTouch: function(touch) {},
            onUpdate: function(){},
            init:function() {
                this.guiContainer = new PIXI.Container();
                this.sprite = Game.pool.getSprite('buttonBuildBuildings');

                this.guiContainer.addChild(this.sprite);
                this.guiContainer.position = this.position;

                this.guiContainer.interactive = true;
                this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
                    this.yestoched = true;
                }.bind(this);

                this.guiContainer.mouseup = this.guiContainer.touchend = this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
                    let buildBuildingsButtons = Game.lvl.objGui.filter(function (g) {
                        return g.params.subType === 'buildUnit' || g.params.subType === 'buildBuilding';
                    });
                    if (this.yestoched) {
                        this.yestoched = false;
                        if (buildBuildingsButtons[0].hidden) {
                            buildBuildingsButtons.forEach(function (b) {
                                b.show();
                            });
                        } else {
                            buildBuildingsButtons.forEach(function (b) {
                                b.hide();
                            })
                        }
                    }
                }.bind(this);

                this.hide = function () {
                    this.startAnimation({startX: this.params.x, toX: this.params.x, startY: this.params.y, toY: this.params.y + 356, iterations:5}, function() {})
                }
            }
        };

        // Game.guiList.builUnitsButton = {
        //     noLock:false,
        //     sprite : null,
        //     x: 64+96,
        //     y: Game.windowHeight-64,
        //     width: 64,
        //     height: 64,
        //     onTouch: function(touch) {},
        //     onUpdate: function(){},
        //     init:function() {
        //         this.guiContainer = new PIXI.Container();
        //         this.sprite = Game.pool.getSprite('buttonBuildUnits');
        //
        //         this.guiContainer.addChild(this.sprite);
        //         this.guiContainer.position = this.position;
        //
        //         this.guiContainer.interactive = true;
        //         this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
        //             this.yestoched = true;
        //         }.bind(this);
        //
        //         this.guiContainer.mouseup = this.guiContainer.touchend = this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
        //             let buildBuildingsButtons = Game.lvl.objGui.filter(function (g) {
        //                 return g.params.subType === 'buildBuilding';
        //             });
        //             let buildUnitsButtons = Game.lvl.objGui.filter(function (g) {
        //                 return g.params.subType === 'buildUnit';
        //             });
        //             if (this.yestoched) {
        //                 this.yestoched = false;
        //                 if (buildUnitsButtons[0].hidden) {
        //                     buildUnitsButtons.forEach(function (b) {
        //                         b.show();
        //                     });
        //                     buildBuildingsButtons.forEach(function (b) {
        //                         b.hide();
        //                     });
        //                 } else {
        //                     buildUnitsButtons.forEach(function (b) {
        //                         b.hide();
        //                     });
        //                 }
        //             }
        //         }.bind(this);
        //
        //         this.hide = function () {
        //             this.startAnimation({startX: this.params.x, toX: this.params.x, startY: this.params.y, toY: this.params.y + 356, iterations:5}, function() {})
        //         }
        //     }
        // };

        Game.guiList.buildTransformer = {
            noLock: false,
            subType: 'buildBuilding',
            building: 'transformer',
            sprite : null,
            x: 64,
            y: Game.windowHeight-64-96,
            width: 64,
            height: 64,
            onTouch: function(touch) {},
            onUpdate: function(){},
            init:function() {
                this.onUpdate = checkPrice.bind(this);
                this.hidden = false;
                this.guiContainer = new PIXI.Container();
                this.sprite = Game.pool.getSprite('buttonBuildTransformer');

                this.guiContainer.addChild(this.sprite);
                this.guiContainer.position = this.position;

                this.price = getPriceText(Game.buildingsList[this.params.building].cost, '#FFFFFF');
                this.guiContainer.addChild(this.price);

                this.guiContainer.interactive = true;
                this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
                    Game.lvl.map.setBuild(this.position.clone(), Game.buildingsList[this.params.building]);
                    this.yestoched = true;
                    Game.lvl.objGui.filter(function (g) {
                        return g.params.subType === 'buildUnit' || g.params.subType === 'buildBuilding';
                    }).forEach(function (b) {
                        b.hide();
                    });
                }.bind(this);

                this.guiContainer.mouseup = this.guiContainer.touchend = this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
                    if (this.yestoched) {
                        Game.lvl.map.dropPhantom();
                    }
                }.bind(this);

                this.hide = function () {
                    if (!this.hidden)
                        this.startAnimation({startX: this.params.x, toX: this.params.x - 256, startY: this.params.y, toY: this.params.y, iterations:5}, function() {
                            this.hidden = true;
                        }.bind(this))
                };

                this.show = function () {
                    if (this.hidden)
                        this.startAnimation({startX: this.params.x - 256, toX: this.params.x, startY: this.params.y, toY: this.params.y, iterations:5}, function() {
                            this.hidden = false;
                        }.bind(this))
                };

                // this.hide();
            }
        };

        Game.guiList.buildBasicTurret = {
            noLock: false,
            subType: 'buildBuilding',
            building: 'basicTurret',
            sprite : null,
            x: 64,
            y: Game.windowHeight-64-96*2,
            width: 64,
            height: 64,
            onTouch: function(touch) {},
            onUpdate: function() {
            },
            init:function() {
                this.onUpdate = checkPrice.bind(this);
                this.hidden = false;
                this.guiContainer = new PIXI.Container();
                this.sprite = Game.pool.getSprite('buttonBuildBasicTurrer');

                this.guiContainer.addChild(this.sprite);

                this.price = getPriceText(Game.buildingsList[this.params.building].cost, '#FFFFFF');
                this.guiContainer.addChild(this.price);

                this.guiContainer.position = this.position;

                this.guiContainer.interactive = true;
                this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
                    Game.lvl.map.setBuild(this.position.clone(), Game.buildingsList[this.params.building]);
                    this.yestoched = true;
                    Game.lvl.objGui.filter(function (g) {
                        return g.params.subType === 'buildUnit' || g.params.subType === 'buildBuilding';
                    }).forEach(function (b) {
                        b.hide();
                    });
                }.bind(this);

                this.guiContainer.mouseup = this.guiContainer.touchend = this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
                    if (this.yestoched) {
                        Game.lvl.map.dropPhantom();
                    }
                }.bind(this);

                this.hide = function () {
                    if (!this.hidden)
                        this.startAnimation({startX: this.params.x, toX: this.params.x - 256, startY: this.params.y, toY: this.params.y, iterations:5}, function() {
                            this.hidden = true;
                        }.bind(this))
                };

                this.show = function () {
                    if (this.hidden)
                        this.startAnimation({startX: this.params.x - 256, toX: this.params.x, startY: this.params.y, toY: this.params.y, iterations:5}, function() {
                            this.hidden = false;
                        }.bind(this))
                };

                // this.hide();
            }
        };

        Game.guiList.buildTeslaTurret = {
            noLock: false,
            subType: 'buildBuilding',
            building: 'teslaTurret',
            sprite : null,
            x: 64,
            y: Game.windowHeight-64-96*3,
            width: 64,
            height: 64,
            onTouch: function(touch) {},
            onUpdate: function(){},
            init:function() {
                this.onUpdate = checkPrice.bind(this);
                this.hidden = false;
                this.guiContainer = new PIXI.Container();
                this.sprite = Game.pool.getSprite('buttonBuildTeslaTurret');

                this.guiContainer.addChild(this.sprite);
                this.guiContainer.position = this.position;

                this.price = getPriceText(Game.buildingsList[this.params.building].cost, '#FFFFFF');
                this.guiContainer.addChild(this.price);


                this.guiContainer.interactive = true;
                this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
                    Game.lvl.map.setBuild(this.position.clone(), Game.buildingsList[this.params.building]);
                    this.yestoched = true;
                    Game.lvl.objGui.filter(function (g) {
                        return g.params.subType === 'buildUnit' || g.params.subType === 'buildBuilding';
                    }).forEach(function (b) {
                        b.hide();
                    });
                }.bind(this);

                this.guiContainer.mouseup = this.guiContainer.touchend = this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
                    if (this.yestoched) {
                        Game.lvl.map.dropPhantom();
                    }
                }.bind(this);

                this.hide = function () {
                    if (!this.hidden)
                        this.startAnimation({startX: this.params.x, toX: this.params.x - 256, startY: this.params.y, toY: this.params.y, iterations:5}, function() {
                            this.hidden = true;
                        }.bind(this))
                };

                this.show = function () {
                    if (this.hidden)
                        this.startAnimation({startX: this.params.x - 256, toX: this.params.x, startY: this.params.y, toY: this.params.y, iterations:5}, function() {
                            this.hidden = false;
                        }.bind(this))
                };

                // this.hide();
            }
        };


        Game.guiList.buildCannonTurret = {
            noLock: false,
            subType: 'buildBuilding',
            building: 'cannonTurret',
            sprite : null,
            x: 64,
            y: Game.windowHeight-64-96*3,
            width: 64,
            height: 64,
            onTouch: function(touch) {},
            onUpdate: function() {},
            init:function() {
                this.onUpdate = checkPrice.bind(this);
                this.hidden = false;
                this.guiContainer = new PIXI.Container();
                this.sprite = Game.pool.getSprite('buttonBuildCannonTurret');

                this.guiContainer.addChild(this.sprite);
                this.guiContainer.position = this.position;

                this.price = getPriceText(Game.buildingsList[this.params.building].cost, '#FFFFFF');
                this.guiContainer.addChild(this.price);

                this.guiContainer.interactive = true;
                this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
                    Game.lvl.map.setBuild(this.position.clone(), Game.buildingsList[this.params.building]);
                    this.yestoched = true;
                    Game.lvl.objGui.filter(function (g) {
                        return g.params.subType === 'buildUnit' || g.params.subType === 'buildBuilding';
                    }).forEach(function (b) {
                        b.hide();
                    });
                }.bind(this);

                this.guiContainer.mouseup = this.guiContainer.touchend = this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
                    if (this.yestoched) {
                        Game.lvl.map.dropPhantom();
                    }
                }.bind(this);

                this.hide = function () {
                    if (!this.hidden)
                        this.startAnimation({startX: this.params.x, toX: this.params.x - 256, startY: this.params.y, toY: this.params.y, iterations:5}, function() {
                            this.hidden = true;
                        }.bind(this))
                };

                this.show = function () {
                    if (this.hidden)
                        this.startAnimation({startX: this.params.x - 256, toX: this.params.x, startY: this.params.y, toY: this.params.y, iterations:5}, function() {
                            this.hidden = false;
                        }.bind(this))
                };
            }
        };

        Game.guiList.buildJeepFabric = {
            noLock: false,
            subType: 'buildUnit',
            building: 'jeepFabric',
            sprite : null,
            x: 64+96,
            y: Game.windowHeight-64-96,
            width: 64,
            height: 64,
            onTouch: function(touch) {},
            onUpdate: function() {},
            init:function() {
                this.onUpdate = checkPrice.bind(this);
                this.hidden = false;
                this.guiContainer = new PIXI.Container();
                this.sprite = Game.pool.getSprite('buttonBuildJeepFabric');

                this.guiContainer.addChild(this.sprite);
                this.guiContainer.position = this.position;

                this.price = getPriceText(Game.buildingsList[this.params.building].cost, '#FFFFFF');
                this.guiContainer.addChild(this.price);

                this.guiContainer.interactive = true;
                this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
                    Game.lvl.map.setBuild(this.position.clone(), Game.buildingsList[this.params.building]);
                    this.yestoched = true;
                    Game.lvl.objGui.filter(function (g) {
                        return g.params.subType === 'buildUnit' || g.params.subType === 'buildBuilding';
                    }).forEach(function (b) {
                        b.hide();
                    });
                }.bind(this);

                this.guiContainer.mouseup = this.guiContainer.touchend = this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
                    if (this.yestoched) {
                        Game.lvl.map.dropPhantom();
                    }
                }.bind(this);

                this.hide = function () {
                    if (!this.hidden)
                        this.startAnimation({startX: this.params.x, toX: this.params.x - 256, startY: this.params.y, toY: this.params.y, iterations:5}, function() {
                            this.hidden = true;
                        }.bind(this))
                };

                this.show = function () {
                    if (this.hidden)
                        this.startAnimation({startX: this.params.x - 256, toX: this.params.x, startY: this.params.y, toY: this.params.y, iterations:5}, function() {
                            this.hidden = false;
                        }.bind(this))
                };

                // this.hide();
            }
        };

        Game.guiList.buildTankFabric = {
            noLock: false,
            subType: 'buildUnit',
            building: 'tankFabric',
            sprite : null,
            x: 64+96,
            y: Game.windowHeight-64-96*2,
            width: 64,
            height: 64,
            onTouch: function(touch) {},
            onUpdate: function() {},
            init:function() {
                this.onUpdate = checkPrice.bind(this);
                this.hidden = false;
                this.guiContainer = new PIXI.Container();
                this.sprite = Game.pool.getSprite('buttonBuildTankFabric');

                this.guiContainer.addChild(this.sprite);
                this.guiContainer.position = this.position;

                this.price = getPriceText(Game.buildingsList[this.params.building].cost, '#FFFFFF');
                this.guiContainer.addChild(this.price);

                this.guiContainer.interactive = true;
                this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
                    Game.lvl.map.setBuild(this.position.clone(), Game.buildingsList[this.params.building]);
                    this.yestoched = true;
                    Game.lvl.objGui.filter(function (g) {
                        return g.params.subType === 'buildUnit' || g.params.subType === 'buildBuilding';
                    }).forEach(function (b) {
                        b.hide();
                    });
                }.bind(this);

                this.guiContainer.mouseup = this.guiContainer.touchend = this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
                    if (this.yestoched) {
                        Game.lvl.map.dropPhantom();
                    }
                }.bind(this);

                this.hide = function () {
                    if (!this.hidden)
                        this.startAnimation({startX: this.params.x, toX: this.params.x - 256, startY: this.params.y, toY: this.params.y, iterations:5}, function() {
                            this.hidden = true;
                        }.bind(this))
                };

                this.show = function () {
                    if (this.hidden)
                        this.startAnimation({startX: this.params.x - 256, toX: this.params.x, startY: this.params.y, toY: this.params.y, iterations:5}, function() {
                            this.hidden = false;
                        }.bind(this))
                };

                // this.hide();
            }
        };

        Game.guiList.map = {
            noLock: true,
            sprite : null,
            x: Game.windowWidth/2,
            y: Game.windowHeight/2,
            width: Game.windowWidth,
            height: Game.windowHeight,
            onTouch: function(touch) {},
            onUpdate: function(){
                if (this.drop) {
                    this.drop = false;
                    if (this.dropSuccess && this.phantom) {
                        let position = new Vec2(Game.touchList[0].lastPosition.x, Game.touchList[0].lastPosition.y);
                        Game.lvl.globalLogic.build(this.phantom.building, Game.lvl.camera.fromGuiLayerToMapLayer(position));
                    }
                    Game.lvl.globalLogic.mainPlayer.buildings.forEach(function (bl) {
                        if (bl.transformer)
                            bl.transformerRange.alpha = 0;
                    });
                    this.phantom && Game.pool.pull(this.phantom.sprite);
                    this.phantom && Game.pool.pull(this.phantom.container);
                    this.phantom = null;
                } else if (this.phantom && Game.touchList[0]) {
                    this.phantom.container.position.set(Game.touchList[0].lastPosition.x, Game.touchList[0].lastPosition.y);
                }
            },
            init:function(params) {
                this.setBuild = params.setBuild;
                this.guiContainer = new PIXI.Container();
                this.guiContainer.pivot.x = Game.windowWidth/2;
                this.guiContainer.pivot.y = Game.windowHeight/2;
                this.guiContainer.position = this.position;

                this.visibleParts = new PIXI.Graphics();
                this.visibleParts.beginFill('rgba(0, 0, 0)', 0.);
                this.visibleParts.drawRect(0, 0, this.width, this.height);
                this.visibleParts.endFill();
                this.guiContainer.addChild(this.visibleParts);

                this.touchedDown = false;

                this.guiContainer.interactive = true;
                this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
                    this.touchedDown = false;
                }.bind(this);
                this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
                    this.touchedDown = true;
                }.bind(this);

                this.guiContainer.mouseup = this.guiContainer.touchend =  function () {
                    if (this.touchedDown) {
                        if (!this.regroupPoint) {
                            this.regroupPoint = Game.pool.getSprite('regroupPoint');
                            Game.lvl.effectContainer.addChild(this.regroupPoint);
                        }
                        let position = new Vec2(Game.touchList[0].lastPosition.x, Game.touchList[0].lastPosition.y);
                        this.regroupPoint.position = Game.lvl.camera.fromGuiLayerToMapLayer(position);
                        Game.lvl.globalLogic.setRegroupTo(this.regroupPoint.position);
                        this.touchedDown = false;
                    }
                    this.dropSuccess = true;
                }.bind(this);

                this.dropPhantom = function () {
                    this.drop = true;
                }
            }, 
            setBuild: function (position, building) {
                this.phantom && Game.pool.pull(this.phantom.sprite);
                this.phantom && Game.pool.pull(this.phantom.container);
                this.dropSuccess = false;
                Game.lvl.globalLogic.mainPlayer.buildings.forEach(function (bl) {
                    if (bl.transformer)
                        bl.transformerRange.alpha = 1;
                });
                this.phantom = new Phantom(building, position);
                this.phantom.container.scale.x = this.phantom.container.scale.y = Game.lvl.camera.getScale();
                this.guiContainer.addChild(this.phantom.container);
            }
        };
        resolve();
    });
}